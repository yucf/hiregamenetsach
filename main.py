# main.py

from mainRoutes.requests import confirmApp, createApplication, login
def main():
    email = 'yfrikhat@gmail.com'
    password = '2&694lZUv&Ig'
    response = login(email, password)
    if response.status_code == 200:
        data = response.json()
    else:
        print("Login failed with status code:", response.status_code)

    createApp_params = {
        'token': data['token'],
        'email': data['email'],
        'first_name': data['first_name'] or 'Youssef',
        'last_name': data['last_name'] or 'FRIKHAT'
    }
    createApp_response = createApplication(createApp_params)

    confirmApp_response = confirmApp(createApp_response.get('confirmation_url'), data['token'])
    
    print('Done!')
    print(confirmApp_response.json())

if __name__ == "__main__":
    main()
