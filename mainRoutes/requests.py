import requests


def login(email, password):
    url = 'https://hire-game.netsach.dev/api/v1.1/auth/login/'
    data = {
        'email': email,
        'password': password
    }
    session = requests.Session()
    response = session.post(url, data=data, allow_redirects=False)

    while response.is_redirect:
        redirect_url = response.headers.get("Location")
        if redirect_url:
            response = session.request("POST", redirect_url, data=data, allow_redirects=False)

    return response

def createApplication(data):
    token = data['token']
    url = 'https://hire-game.netsach.dev/api/v1.1/job-application-request/?c_auth_with_token='+ token
    email = data['email']
    first_name = data['first_name']
    last_name = data['last_name']

    headers = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
    data = {
        'email': email,
        'first_name': first_name,
        'last_name': last_name
    }
    session = requests.Session()
    response = session.post(url, data=data, allow_redirects=False)

    while response.is_redirect:
        redirect_url = response.headers.get("Location")
        if redirect_url:
            response = session.request("POST", redirect_url, data=data, allow_redirects=False)
    response = response.json()
    
    while True:
        newResponse = requests.get(response.get('url') + f"?c_auth_with_token={token}", headers=headers)
        newResponse = newResponse.json()
        if newResponse.get('status') == 'COMPLETED':  
            break
    return newResponse

def confirmApp(url, token):
    data = {
        'confirmed': True,
    }

    session = requests.Session()
    response = session.patch(url + f"?c_auth_with_token={token}", data=data, allow_redirects=False)

    while response.is_redirect:
        redirect_url = response.headers.get("Location")
        if redirect_url:
            response = session.request("PATCH", redirect_url, data=data, allow_redirects=False)
    return response  




